# Specify parent image. Please select a fixed tag here.
ARG BASE_IMAGE=registry.git.rwth-aachen.de/jupyter/profiles/rwth-courses:latest
FROM ${BASE_IMAGE}
#USER root 
# install mesa
#RUN apt-get update 
#RUN apt install libgl1-mesa-glx -y

#USER jovyan
RUN apt-get update 
RUN apt install graphviz




# Install packages via requirements.txt
ADD requirements.txt .
RUN pip install -r requirements.txt

# Install mamba, I am sure this not how this is suppoed to work 
# RUN wget "https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-$(uname)-$(uname -m).sh" bash Miniforge3-$(uname)-$(uname -m).sh

# .. Or update conda base environment to match specifications in environment.yml
ADD ifcopenshell.yml /tmp/environment.yml

# All packages specified in environment.yml are installed in the base environment
RUN mamba env update -f /tmp/environment.yml && \
    mamba clean -a -f -y
RUN jupyter labextension install jupyter-threejs
RUN jupyter labextension install jupyter-datawidgets
RUN jupyter labextension install ipycanvas

